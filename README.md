## NOTE: This version of the AR Drone 2.0 Simulink target is meant only for use with the Integration Project course of the Eindhoven University of Technology.

# AR Drone 2.0 MATLAB/Simulink 2.0 Target 

This Simulink Project provides automatic code generation support for Simulink models for the Parrot AR Drone 2.0. 
The provided Simulink blocks allow you to read all the sensors and both cameras on the AR Drone 2.0. You can also control the motors to make the AR Drone 2.0 hover.
Additionally for the integration project, models are provided in which estimators and controllers can be inserted.

## Before you can use this toolbox
1. Download and install MATLAB R2016b or higher for Windows. You also need a number of toolboxes, listed at the bottom of this page.
2. Install a C compiler. You can evaluate `mex -setup c` in MATLAB to see if you have a compiler installed. The free MinGW64 compiler can be installed from the MATLAB Add-Ons Explorer
3. Download and install the Code Sourcery ARM compiler. This is a free compiler which you can download using [this direct link.](https://sourcery.mentor.com/sgpp/lite/arm/portal/package8738/public/arm-none-linux-gnueabi/arm-2011.03-41-arm-none-linux-gnueabi.exe). If you are using Windows 8 or newer you will need to run the installer in the Windows 7 compatibility mode

## How to open this toolbox
### Method 1: Using Simulink
1. Select New in the MATLAB menu bar, then Simulink Project. This will open up the Simulink Start Page.
2. On the bottom left, under Simulink Project, select Source Control... This will open the Simulink Project - Project Retriever.
3. Change the Source control integration to Git. Set the Repository path to https://SanneMarx@gitlab.com/ARDrone/IntegrationProject.git
4. The Sandbox folder is where the AR Drone 2.0 Simulink Target will be stored on your PC, set this to an empty folder of your choice.
5. Click Retrieve to fetch the files from the online GitLab source.

### Method 2: Direct Download
1. Use the _Download Zip_ button on this page
2. Extract the zip to a destination of your choice
3. Navigate to the folder where you extracted the toolbox in MATLAB and open AR_Drone.prj

## How to use the Simulink project
1. Opening AR_Drone.prj will cause a startup script to set up MATLAB for the toolbox. Closing the project disables the toolbox functionality
2. The Simulink project provides you with shortcuts on the MATLAB toolstrip. Things such as the documentation or calibration models can be easily accessed from there
3. The folder structure for the files is such that
  1. Pre made Simulink models are inside the AR_Drone_Models folder
  2. Library blocks, C source code and custom scripts are in the AR_Drone_Target folder 
4. The documentation will automatically open when the Simulink project is openened. It contains background information, examples and troubleshooting awnsers.

## Before you can connect to the AR Drone 2.0
A WIFI connection has to be made between the AR Drone 2.0 and your Simulink PC. This can be blocked by a firewall / virus scanner. To solve this:
1. Fully dissable your firewall.
2. White list the AR Drone 2.0 in your firewall. The default IP is 192.168.1.1

## Required toolboxes:
It is recommended to install all toolboxes available to you for ease of installation. The minimum required toolboxes for the full AR Drone 2.0 Target functionality are:
* Aerospace Blockset
* Computer Vision System Toolbox
* Control System Toolbox
* DSP System Toolbox
* Embedded Coder
* Instrument Control Toolbox
* MATLAB Coder
* MATLAB Compiler
* Simulink 3D Animation
* Simulink Coder
* Stateflow
* System Identification Toolbox