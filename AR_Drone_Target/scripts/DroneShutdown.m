% This script defines a project shortcut. 
%
% To get a handle to the current project use the following function:
%
% project = simulinkproject();
%
% You can use the fields of project to get information about the currently 
% loaded project. 
%
% See: help simulinkproject

%% restore warnings - DISABLED as it causes constant out of syncs with GIT
% myDictObj = Simulink.data.dictionary.open('ArDroneDataDictionary.sldd');
% dDataSectObj = getSection(myDictObj,'Other Data');
% origWarningState = dDataSectObj.getEntry('origWarningState').getValue;
% warning(origWarningState);

%% reset build folder
set_param(0, 'CacheFolder', '');
set_param(0, 'CodeGenFolder', '');

bdclose all;
close all;
clear; % this was clear all, was "all" really necessary?
clc;