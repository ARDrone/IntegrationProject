function [ output_args ] = getFromArDict( input_args )
% GETFROMARDICT will retrieve the value of a specified entry in the Ar
% Drone data dictionary.
%
% GETFROMARDICT('all') will list all entries stored in the dictionary
if strcmp(input_args,'all')
    foundEntries = Simulink.data.dictionary.open('ArDroneDataDictionary.sldd').getSection('Design Data').find;
    [n,m] = size(foundEntries);
    output_args = cell(n,m);
    for ii = 1:n
    output_args{ii} = foundEntries(ii).Name;
    end
else
    output_args = Simulink.data.dictionary.open('ArDroneDataDictionary.sldd').getSection('Design Data').getEntry(input_args).getValue;
end
end

