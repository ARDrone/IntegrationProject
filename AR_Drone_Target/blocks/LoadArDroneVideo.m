function LoadArDroneVideo(block)

  setup(block);
  
%endfunction

function setup(block)
   
  %% Register number of input and output ports
  block.NumInputPorts  = 0;
  block.NumOutputPorts = 1;
  block.NumDialogPrms  = 1;
  
  block.OutputPort(1).DatatypeID = 3;
  block.OutputPort(1).SamplingMode = 'sample';
  
  % set the output size
  fileName = block.DialogPrm(1).Data;
  load(fileName);
  block.OutputPort(1).Dimensions = size(ArDroneVideo,2);


  %% Setup functional port properties to dynamically
  %% inherited.
  block.SetPreCompInpPortInfoToDynamic;
  
  %% Set block sample time to inhereted
  block.SampleTimes = [-1 0];
  
  %% Set the block simStateCompliance to default (i.e., same as a built-in block)
  block.SimStateCompliance = 'DefaultSimState';

  %% Register methods
  block.RegBlockMethod('PostPropagationSetup',@DoPostPropSetup);
  block.RegBlockMethod('InitializeConditions',    @InitConditions);  
  block.RegBlockMethod('Outputs',             @Output);  


  %% Set block as a viewer
  block.SetSimViewingDevice(true);
  
%endfunction

function InitConditions(block)

block.Dwork(1).Data = 1;
  
%endfunction

function DoPostPropSetup(block)

  %% Setup Dwork
  block.NumDworks = 1;
  block.Dwork(1).Name = 'frameIndex'; 
  block.Dwork(1).Dimensions      = 1;
  block.Dwork(1).DatatypeID      = 0;
  block.Dwork(1).Complexity      = 'Real';
  block.Dwork(1).UsedAsDiscState = true;
  
%endfunction

function Output(block)

fileName = block.DialogPrm(1).Data;
load(fileName);
frame = block.Dwork(1).Data;
if frame > size(ArDroneVideo,1)
    block.Dwork(1).Data = 1;
    frame = 1;
end

block.OutputPort(1).Data = ArDroneVideo(frame,:);
block.Dwork(1).Data = block.Dwork(1).Data + 1;
%endfunction

