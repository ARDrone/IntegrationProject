#ifndef MATLAB_MEX_FILE
#include <time.h>
#include <stdio.h>	
#include <stdint.h>
#endif //MATLAB_MEX_FILE

#ifndef MATLAB_MEX_FILE
#endif //MATLAB_MEX_FILE

void ArClockInit(void){
#ifndef MATLAB_MEX_FILE
#endif //MATLAB_MEX_FILE
}
void ArClockClose(void){
#ifndef MATLAB_MEX_FILE
#endif //MATLAB_MEX_FILE
}

void ArClockStep(double *time){
#ifndef MATLAB_MEX_FILE
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    *time = (double)(now.tv_sec * 1000000000 + now.tv_nsec);
#endif //MATLAB_MEX_FILE
}

//For use in MATLAB ceval calls
double ArClock(void){
#ifndef MATLAB_MEX_FILE
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    return (double)(now.tv_sec * 1000000000 + now.tv_nsec);
#endif //MATLAB_MEX_FILE
}